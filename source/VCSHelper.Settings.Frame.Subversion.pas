{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-27  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit VCSHelper.Settings.Frame.Subversion;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   ExtCtrls,
   IDEOptEditorIntf,
   IDEOptionsIntf;

type

   { TVCSSettingsSubversionUI }

   TVCSSettingsSubversionUI = class(TAbstractIDEOptionsEditor)
      cbShow: TCheckBox;
      cbtCommitOnProjectClose: TCheckBox;
      groupGeneral: TGroupBox;
      rgCloseOnEnd: TRadioGroup;
   private
      { private declarations }
   public
      { public declarations }
      constructor Create(AOwner: TComponent); override;
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

uses
   VCSHelper.Options,
   VCSHelper.Strings;

{$R *.lfm}

{ TVCSSettingsSubversionUI }

constructor TVCSSettingsSubversionUI.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
end;

function TVCSSettingsSubversionUI.GetTitle: string;
begin
   Result := 'Subversion';
end;

procedure TVCSSettingsSubversionUI.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbShow.Caption := rsVCSOptionShowInMainMenu;
   cbtCommitOnProjectClose.Caption := rsVCSOptionCommitOnProjectClose;
end;

procedure TVCSSettingsSubversionUI.ReadSettings(AOptions: TAbstractIDEOptions);
begin
   cbShow.Checked := VCSSettings.SubversionSettings.ShowInMainMenu;
   cbtCommitOnProjectClose.Checked := VCSSettings.SubversionSettings.CommitOnProjectClose;
   rgCloseOnEnd.ItemIndex := VCSSettings.SubversionSettings.CloseOnEnd;
end;

procedure TVCSSettingsSubversionUI.WriteSettings(AOptions: TAbstractIDEOptions);
begin
   VCSSettings.SubversionSettings.ShowInMainMenu := cbShow.Checked;
   VCSSettings.SubversionSettings.CommitOnProjectClose := cbtCommitOnProjectClose.Checked;
   VCSSettings.SubversionSettings.CloseOnEnd := rgCloseOnEnd.ItemIndex;
end;

class function TVCSSettingsSubversionUI.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TVCSSettings;
end;

initialization

   RegisterIDEOptionsEditor(VCSOptionGroup, TVCSSettingsSubversionUI, 5);

end.

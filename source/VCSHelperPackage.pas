{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit VCSHelperPackage;

interface

uses
   VCSHelper.Options, VCSHelper.Settings.Frame.Bazaar, 
   VCSHelper.Settings.Frame.Git, VCSHelper.Settings.Frame.Mercurial, 
   VCSHelper.Settings.Frame.Subversion, VCSHelper.Strings, VCSHelper.Menu, 
   LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('VCSHelper.Menu', @VCSHelper.Menu.Register);
end;

initialization
  RegisterPackage('VCSHelperPackage', @Register);
end.

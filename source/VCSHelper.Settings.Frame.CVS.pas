{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-27  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit VCSHelper.Settings.Frame.CVS;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   IDEOptionsIntf;

type

   { TVCSSettingsCVSUI }

   TVCSSettingsCVSUI = class(TAbstractIDEOptionsEditor)
      cbShow: TCheckBox;
      cbtCommitOnProjectClose: TCheckBox;
      groupGeneral: TGroupBox;
   private
      { private declarations }
   public
      { public declarations }
      constructor Create(AOwner: TComponent); override;
      function GetTitle: string; override;
      procedure Setup(ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings(AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings(AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

uses
   VCSHelper.Options,
   VCSHelper.Strings;

{$R *.lfm}

{ TVCSSettingsCVSUI }

constructor TVCSSettingsCVSUI.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
end;

function TVCSSettingsCVSUI.GetTitle: string;
begin
   Result := 'CVS';
end;

procedure TVCSSettingsCVSUI.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbShow.Caption := rsVCSOptionShowInMainMenu;
   cbtCommitOnProjectClose.Caption := rsVCSOptionCommitOnProjectClose;
end;

procedure TVCSSettingsCVSUI.ReadSettings(AOptions: TAbstractIDEOptions);
begin
   cbShow.Checked := VCSSettings.CVSSettings.ShowInMainMenu;
   cbtCommitOnProjectClose.Checked := VCSSettings.CVSSettings.CommitOnProjectClose;
end;

procedure TVCSSettingsCVSUI.WriteSettings(AOptions: TAbstractIDEOptions);
begin
   VCSSettings.CVSSettings.ShowInMainMenu := cbShow.Checked;
   VCSSettings.CVSSettings.CommitOnProjectClose := cbtCommitOnProjectClose.Checked;
end;

class function TVCSSettingsCVSUI.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TVCSSettings;
end;

initialization
   // RegisterIDEOptionsEditor(VCSOptionGroup, TVCSSettingsCVSUI, 2);
end.

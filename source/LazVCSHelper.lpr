program LazVCSHelper;

{$mode objfpc}{$H+}

uses
   Interfaces,
   Forms,
   GuiTestRunner,
   fpcunittestrunner,
   VCSHelper.Settings.Frame.Bazaar,
   VCSHelper.Settings.Frame.Git,
   VCSHelper.Settings.Frame.Mercurial,
   VCSHelper.Settings.Frame.Subversion,
   VCSHelper.Menu,
   VCSHelper.Options,
   VCSHelper.Strings,
   PepiMK.VCS.Git.LibraryAPI.TestCase;

{$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TGuiTestRunner, TestRunner);
   Application.Run;
end.

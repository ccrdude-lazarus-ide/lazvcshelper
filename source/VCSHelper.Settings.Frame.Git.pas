{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Interface to Git Version Control System.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-03-27  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
// https://tortoisegit.org/docs/tortoisegit/tgit-automation.html
// *****************************************************************************
   )
}

unit VCSHelper.Settings.Frame.Git;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   ExtCtrls,
   IDEOptEditorIntf,
   IDEOptionsIntf;

type

   { TVCSSettingsGitUI }

   TVCSSettingsGitUI = class(TAbstractIDEOptionsEditor)
      cbShow: TCheckBox;
      cbtCommitOnProjectClose: TCheckBox;
      groupGeneral: TGroupBox;
      rgCloseOnEnd: TRadioGroup;
   private
      { private declarations }
   public
      { public declarations }
      constructor Create(AOwner: TComponent); override;
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

uses
   VCSHelper.Options,
   VCSHelper.Strings;

{$R *.lfm}

{ TVCSSettingsGitUI }

constructor TVCSSettingsGitUI.Create(AOwner: TComponent);
begin
   inherited Create(AOwner);
end;

function TVCSSettingsGitUI.GetTitle: string;
begin
   Result := 'Git';
end;

procedure TVCSSettingsGitUI.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   cbShow.Caption := rsVCSOptionShowInMainMenu;
   cbtCommitOnProjectClose.Caption := rsVCSOptionCommitOnProjectClose;
end;

procedure TVCSSettingsGitUI.ReadSettings(AOptions: TAbstractIDEOptions);
begin
   cbShow.Checked := VCSSettings.GitSettings.ShowInMainMenu;
   cbtCommitOnProjectClose.Checked := VCSSettings.GitSettings.CommitOnProjectClose;
   rgCloseOnEnd.ItemIndex := VCSSettings.GitSettings.CloseOnEnd;
end;

procedure TVCSSettingsGitUI.WriteSettings(AOptions: TAbstractIDEOptions);
begin
   VCSSettings.GitSettings.ShowInMainMenu := cbShow.Checked;
   VCSSettings.GitSettings.CommitOnProjectClose := cbtCommitOnProjectClose.Checked;
   VCSSettings.GitSettings.CloseOnEnd := rgCloseOnEnd.ItemIndex;
end;

class function TVCSSettingsGitUI.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TVCSSettings;
end;

initialization

   RegisterIDEOptionsEditor(VCSOptionGroup, TVCSSettingsGitUI, 3);

end.
